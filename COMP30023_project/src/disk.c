#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include "utils.h"
#include "disk.h"

FILE *disk_image = NULL;
int disk_image_size = 0;

int file_size(FILE *fp);

int make_disk(char *name, unsigned int size)
{
	if(size < MINIMUM_DISK_SIZE || size > MAXIMUM_DISK_SIZE)
	{
		return -1;
	}
	FILE *fp = fopen(name, "wb");				// Create a new file with the name "name"
	assert(fp != NULL);							// Check whether fopen succeed
	char blank_block[DISK_BLOCK_SIZE];
	memset(&blank_block, 0xe5, DISK_BLOCK_SIZE);// Initialize all slots of the array to 0xe5
	int total_written = 0;
	while(total_written < (int)size)			// while loop to write blank_block in clusters/blocks(?) with size of DISK_BLOCK_SIZE
	{
		size_t written = 0;
		written = fwrite(&blank_block, sizeof(char), DISK_BLOCK_SIZE, fp);
		assert(written == DISK_BLOCK_SIZE);
		total_written += (int)written;
	}
	fclose(fp);
	return total_written;
}

int open_disk(char *name)
{
	disk_image = fopen(name, "rb+");												// open binary file with read+write permission
	disk_image_size = file_size(disk_image);
	if(disk_image_size % DISK_BLOCK_SIZE != 0)										// The disk needs to be in blocks of size DISK_BLOCK_SIZE
	{
		return -1;
	}
	if(disk_image_size < MINIMUM_DISK_SIZE || disk_image_size > MAXIMUM_DISK_SIZE)
	{
		return -1;
	}
	debug_printf("opened disk image %s\n", name);
	return disk_image_size;
}

int close_disk()
{
	if(disk_image == NULL)
	{
		return -1;
	}
	fclose(disk_image);
	disk_image = NULL;
	debug_printf("closed disk image\n");
	return 0;
}

int read_block(int block, void *buf)
{
	assert(disk_image != NULL);
	long offset = block * DISK_BLOCK_SIZE;											// offset of the block position
	assert(offset + DISK_BLOCK_SIZE < disk_image_size && offset >= 0);				// check whether there is a block in the wanted position
	if(fseek(disk_image, offset, SEEK_SET) != 0)									// set the -stream- position to the wanted block.
																					// SEEK_SET = calculate the offset relative to the start of the file
	{
		return -1;
	}
	debug_printf("reading from block %u, offset %x\n", block, offset);
	uint8_t disk_block[DISK_BLOCK_SIZE];
	size_t rval = fread(&disk_block, sizeof(uint8_t), DISK_BLOCK_SIZE, disk_image);	// read the block from the stream position
	assert(rval == DISK_BLOCK_SIZE);												// checks whether fread reads a whole block
	memcpy(buf, &disk_block, DISK_BLOCK_SIZE);										// copy the content of the block to the buffer buf
	return DISK_BLOCK_SIZE;
}

int write_block(int block, const void *buf)
{
	assert(disk_image != NULL);
	long offset = block * DISK_BLOCK_SIZE;												// offset of the block position
	assert(offset + DISK_BLOCK_SIZE < disk_image_size && offset >= 0);					// check whether there is a block in the wanted position
	int rval = fseek(disk_image, offset, SEEK_SET);										// set the -stream position to the wanted block
	assert(rval != -1);
	debug_printf("writing to block %u, offset %x\n", block, offset);
	uint8_t disk_block[DISK_BLOCK_SIZE];
	memcpy(&disk_block, buf, DISK_BLOCK_SIZE);											// write the buffer to the local variable block
	size_t rval2 = fwrite(&disk_block, sizeof(uint8_t), DISK_BLOCK_SIZE, disk_image);	// write the local variable block to the disk
	assert(rval2 == DISK_BLOCK_SIZE);
	fflush(disk_image);																	// forces the write of all buffered data (see man page)
	return DISK_BLOCK_SIZE;
}

int disk_size()
{
	return disk_image_size;
}

int file_size(FILE *fp)
{
	assert(fp != NULL);
	//save original location
	long original_pos = ftell(fp);
	assert(original_pos != -1);
	//seek to end of file
	fseek(fp, 0, SEEK_END);
	long size = ftell(fp);
	assert(size != -1);
	//seek back to original location
	fseek(fp, original_pos, SEEK_SET);
	return (int)size;
}
